/*function manageConsent() {
    if (localStorage.getItem("cookieConsent") == null ) {
        localStorage.setItem("cookieConsent", "false")
        document.getElementById("cookieBanner").style["display"] = "block"
    } else {
        cookieConsentValue = localStorage.getItem("cookieConsent")

        if (cookieConsentValue == "true") {
            body = document.getElementsByTagName("body")[0];
            s = document.createElement("script");
            s.src = "https://www.googletagmanager.com/gtag/js?id="+trackerID;
            body.appendChild(s);

            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', '{{ .Site.Params.google.analytics.trackerID }}');
        }       
    }
}*/

$('document').ready(function(){

	manageConsent()


	tag_bash = document.getElementsByClassName("language-bash")
	var i = 0; for (i=0; i < tag_bash.length; i++) {
  
    		tag_bash[i].style["background-color"] = "#f5f2f0";
	}
});

function cookieClick(event) {
    var cookieButtomID = event.id;

    if (cookieButtomID == "accept") {
        localStorage.setItem("cookieConsent", "true");
    }

    if (cookieButtomID == "deny") {
        localStorage.setItem("cookieConsent", "false");

    }

    location.reload(); 
}
