---
title: "Biography"
date: 2020-04-28T17:30:36+02:00
draft: false
---
He experimented, as a boy, as a self-taught and voluntary apprentice in the world of
technology, especially information technology. Deepens initially the knowledge of
IT context, through the reading and study of specialized magazines, blogs and forums.
Starting from the second year of the secondary school in Computer Science, he devoted himself
mainly in the area of IT networks, from development to management. In particular
through the creation of client-server environments through the use of virtual machines (VM).
It also studies and tests the process that leads to software development through projects
personal, school and university.