---
title: "Come creare in PHP un pagamento futuro con PayPal"
date: 2020-05-05T17:54:09+02:00
draft: false
featured: true
imgsrc: "img/FuturePaymentPayPalIT.png"
---
![FuturePaymentPayPalIT](/img/FuturePaymentPayPalIT.png)  
PayPal permette di creare un pagamento il cui completamento, ossia il prelievo e la ricezione del denaro, avverrà in un secondo momento. In questo articolo verrà spiegato come utilizzare questa funzionalità in una applicazione Web utilizzando PHP.  
<!--more-->

Prima di poter utilizzare le API di PayPal bisogna effettuare la installazione dell'SDK di PayPal che consiste nel suo download dal seguente link [PayPal SDK](https://github.com/paypal/PayPal-PHP-SDK/releases), nello scompattare nella cartella dell'applicazione, inizializzare l'SDK andando a includere il sequente file 
```PHP
require FCPATH  . '/PayPal-PHP-SDK/autoload.php';
```
e nel richiamare i vari oggetti utilizzati all'inizio del file PHP attraverso una direttiva di questo tipo
```PHP 
use PayPal\Api\Payer;
```
### Creazione del pagamento futuro

Affinche si possa utilizzare questa funzionalità bisogna compiere due passi, creare l'oggetto che rappresenta il pagamento e fare il redirect sul sistema di PayPal in modo che l'acquirente fornisca l'autorizzazione.  
Per creare l'oggetto è necessario prima creare l'istanza delle api, specificando Il ClientID e il ClientSecret che si ricavano dall'area developer di PayPal dopo la creazione di un progetto https://www.paypal.com/signin?intent=developer
```PHP
$apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'ClientID',     // ClientID
                'ClientSecret'      // ClientSecret
            )
);
```
e una serie di istanze di classi di PayPal:
- Un oggetto che rappresenta l'acquirente
```PHP
$payer = new Payer();
$payer -> setPaymentMethod("paypal");
```
- Un oggetto che rappresenta il venditore, dove viene settata l'email associata al suo account
```PHP
$payee = new Payee();
$payee->setEmail("venditore@example.com");
```
- Un oggetto che rappresenta l'ammontare del pagamento, dove si deve specificare la valuta, specificando il [codice stringa](https://developer.paypal.com/docs/api/reference/currency-codes/), e il totale
```PHP
$amount = new Amount();
$amount->setCurrency("EUR")->setTotal("25.00");
```
- Un oggetto che rappresenta la transazione dove specificar l'istanza dell'ammontare del pagamento, la descrizione della transazione, una stringa che la identifica e l'istanza del destinatario del pagamento
```PHP
$transaction = new Transaction();
$transaction->setAmount($amount)->setDescription("Pagamento per camera Basic")->setCustom("Pagamento 001")->setPayee($payee);
```
- Un oggetto che contiene gli url dove fare il redirect in caso di transazione creata con successo o in caso di transazione non creata
```PHP
$redirectUrls = new RedirectUrls();
$redirectUrls->setReturnUrl("http://www.example.com/success")->setCancelUrl("http://www.example.com/failed");
```
Infine si può creare il pagamento futuro
```PHP
$payment = new FuturePayment();
$payment->setIntent("authorize")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

$payment->create($apiContext);
```

**Il codice complessivo è**
```PHP
$apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'ClientID',     // ClientID
                'ClientSecret'      // ClientSecret
            )
);

$payer = new Payer();
$payer->setPaymentMethod("paypal");
$payee = new Payee();
$payee->setEmail("venditore@example.com");
$amount = new Amount();
$amount->setCurrency("EUR")->setTotal("25.00");
$transaction = new Transaction();
$transaction->setAmount($amount)->setDescription("Pagamento per camera Basic")->setCustom("Pagamento 001")->setPayee($payee);
$redirectUrls = new RedirectUrls();
$redirectUrls->setReturnUrl("http://www.example.com/success")->setCancelUrl("http://www.example.com/failed");

$payment = new FuturePayment();
$payment->setIntent("authorize")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

$payment->create($apiContext);
```
Dopo la creazione del pagamento è necessario effettuare il redirect al sito di PayPal in modo tale che l'acquirente fornisca l'autorizzazione per il pagamento. L'url lo si ricava attraverso il seguente metodo 
```PHP
$payment->getApprovalLink();
```

### Ricezione del pagamento 
Anche per poter procedere alla chiusura in positivo del pagamento e, di conseguenza, alla sua ricezione è necessario creare un'istanza delle api, che evitiamo di ripetere, e una serie di oggetti:

- Uno che rappresenta il pagamento specificando l'identificativo di esso ottenuto come parametro di ritorno nell'URL di successo
```PHP
$payment = Payment::get("PAYID-AF46441464AF", $apiContext);
```
- Uno che rappresenta l'esecuzione del pagamento, dove specificare l'identificativo PayPal dell'acquirente, anch'esso ottenuto come parametro nell'URL di successo
```PHP
$execution = new PaymentExecution();
$execution->setPayerId("ADAFSD3443CS3");
```
- Uno che rappresenta l'autorizzazione della transazione
```PHP
try {
  $result = $payment->execute($execution, $apiContext);
  $authorization = $result->getTransactions()[0]->getRelatedResources()[0]->getAuthorization();
} catch (Exception $ex) {
    print_r($ex);
}
```
- Dopo aver ottenuto l'autorizzazione da questa ricavare l'identificativo di essa e il totale
```PHP
$authorizationId = $authorization->getId();
$total = $authorization->getAmount()->getTotal();
```
- Uno che rappresenta l'autorizzazione al pagamento
```PHP
$authorization = Authorization::get($authorizationId, $apiContext);
```
- Uno che rappresenta l'ammontare del pagamento
```PHP
$amt = new Amount();
$amt->setCurrency("EUR")->setTotal($total);
```
- Uno che rappresenta la classe per concludere il pagamento 
```PHP
$capture = new Capture();
$capture->setAmount($amt);
```
Dopo aver effettuato questo il pagamento può essere concluso e il denaro ricevuto
```PHP
$capture = $authorization->capture($capture, $apiContext);
```

**Funzione completa**
```PHP
$payment = Payment::get("PAYID-AF46441464AF", $apiContext);
$execution = new PaymentExecution();
$execution->setPayerId("ADAFSD3443CS3");
$authorization = null;

try {
    $result = $payment->execute($execution, $apiContext);
    $authorization = $result->getTransactions()[0]->getRelatedResources()[0]->getAuthorization();
} catch (Exception $ex) {
    print_r($ex);
}

$authorizationId = $authorization->getId();
$total = $authorization->getAmount()->getTotal();
$authorization = Authorization::get($authorizationId, $apiContext);
$amt = new Amount();
$amt->setCurrency("EUR")->setTotal($total);

$capture = new Capture();
$capture->setAmount($amt);
$capture = $authorization->capture($capture, $apiContext);
```
### Cancellazione della transazione

Per poter cancellare la transazione è necessario creare un'istanza delle api, che evitiamo di ripetere, e una serie di istanze di classi di PayPal:
- Uno che rappresenta il pagamento specificando l'identificativo di esso ottenuto come parametro di ritorno nell'URL di successo
```PHP
$payment = Payment::get("PAYID-AF46441464AF", $apiContext);
```
- Uno che rappresenta l'esecuzione del pagamento, dove specificare l'identificativo PayPal dell'acquirente, anch'esso ottenuto come parametro nell'URL di successo
```PHP
$execution = new PaymentExecution();
$execution->setPayerId("ADAFSD3443CS3");
```
- Una che rappresenta l'autorizzazione della transazione
```PHP
try {
  $result = $payment->execute($execution, $apiContext);
  $authorization = $result->getTransactions()[0]->getRelatedResources()[0]->getAuthorization();
} catch (Exception $ex) {
    print_r($ex);
}
```
- Dopo aver ottenuto l'autorizzazione da questa ricavare l'identificativo di essa
```PHP
$authorizationId = $authorization->getId();
```
- Una che rappresenta l'autorizzazione al pagamento
```PHP
$authorization = Authorization::get($authorizationId, $apiContext);
```

Dopo aver effettuato questo il pagamento può essere annullato
```PHP
$authorization->void($apiContext);
```

**Funzione completa**
```PHP
$payment = Payment::get("PAYID-AF46441464AF", $apiContext);
$execution = new PaymentExecution();
$execution->setPayerId("ADAFSD3443CS3");
$authorization = null;

try {
    $result = $payment->execute($execution, $apiContext);
    $authorization = $result->getTransactions()[0]->getRelatedResources()[0]->getAuthorization();
} catch (Exception $ex) {
    print_r($ex);
}

$authorizationId = $authorization->getId();
$authorization = Authorization::get($authorizationId, $apiContext);
$authorization->void($apiContext);
```