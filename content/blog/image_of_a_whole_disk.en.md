---
title: "Full disk image: creating and opening in Linux"
date: 2021-09-10T16:30:30+02:00
draft: false
featured: true
imgsrc: "img/HugoMultilingualDataFiles.png"
---
<br><br>   
In this guide I will explain how do a full disk image and successively mount it.
<!--more-->
## Create disk image with `dd`

First of all, you have to identify the path of your source disk. You can accomplish this step executing with administrator permissions `fdisk -l` command. 

```bash
sudo fdisk -l
...
Disk /dev/sdb: 238,49 GiB, 256060514304 bytes, 500118192 sectors
Disk model: SanDisk SD8SBAT2
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x16742145

Device     Boot     Start       End   Sectors   Size Id Type
/dev/sdb1  *         2048   1187839   1185792   579M  7 HPFS/NTFS/exFAT
/dev/sdb2         1187840 365901823 364713984 173,9G  7 HPFS/NTFS/exFAT
/dev/sdb3       365901824 449787903  83886080    40G 83 Linux
/dev/sdb4       449787904 500117503  50329600    24G 82 Linux swap / Solaris


Disk /dev/sdc: 931,53 GiB, 1000204886016 bytes, 1953525168 sectors
Disk model: CT1000MX500SSD1 
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 59A18AC5-7D15-AC4B-A37A-E1940988757C

Device     Start        End    Sectors   Size Type
/dev/sdc1   2048 1953519615 1953517568 931,5G Microsoft basic data
...
```

In this example I chose `/dev/sdb`.

For the image creation you have to use 
```bash
sudo dd if=/dev/sdb of=/path_of_image/ssd240gb.img conv=noerror
```
and you wait end.

## Image mounting with `kpartx` software
For the image mount is necessary create a device for every partition, for this you need to use the `kpartx` software. 
Usually this software isn't included in the linux distributions. To install you have to use the package manager, with command 
```bash
sudo apt/yum/dnf install kpartx
```
After installation, you have to execute in this way
```bash
sudo kpartx -a -v /path_of_image/ssd240gb.img
add map loop6p1 (253:0): 0 1185792 linear 7:6 2048
add map loop6p2 (253:1): 0 364713984 linear 7:6 1187840
add map loop6p3 (253:2): 0 83886080 linear 7:6 365901824
add map loop6p4 (253:3): 0 50329600 linear 7:6 449787904
```
How you can see in the output, the sofware have created 4 devices `loop*p*` in path `/dev/mapper/`. This can mounted as any partition, that is
```bash
sudo mount /dev/mapper/loop6p2 /mnt/part1
```
<br><br>   
After ended the work, you have to umount the partition

```bash
umount /mnt/part1
```
and eliminate these devices

```bash
kpartx -d -v /path_of_image/ssd240gb.img
```