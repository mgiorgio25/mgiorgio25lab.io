---
title: "Linux: Creazione e apertura di un immagine di un intero disco"
date: 2021-09-10T16:30:30+02:00
draft: false
featured: true
imgsrc: "img/HugoMultilingualDataFiles.png"
---
<br><br>   
In questa guida vi spiegherò come effettuare un immagine di un intero hard disk o ssd e successivamente montare tale immagine.
<!--more-->
## Creazione immagine del disco con `dd`

Prima di poter creare l'immagine bisogna identificare il percorso del disco, per fare ciò usiamo il comando `fdisk -l` con permessi di amministratore

```bash
sudo fdisk -l
...
Disk /dev/sdb: 238,49 GiB, 256060514304 bytes, 500118192 sectors
Disk model: SanDisk SD8SBAT2
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x16742145

Device     Boot     Start       End   Sectors   Size Id Type
/dev/sdb1  *         2048   1187839   1185792   579M  7 HPFS/NTFS/exFAT
/dev/sdb2         1187840 365901823 364713984 173,9G  7 HPFS/NTFS/exFAT
/dev/sdb3       365901824 449787903  83886080    40G 83 Linux
/dev/sdb4       449787904 500117503  50329600    24G 82 Linux swap / Solaris


Disk /dev/sdc: 931,53 GiB, 1000204886016 bytes, 1953525168 sectors
Disk model: CT1000MX500SSD1 
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 59A18AC5-7D15-AC4B-A37A-E1940988757C

Device     Start        End    Sectors   Size Type
/dev/sdc1   2048 1953519615 1953517568 931,5G Microsoft basic data
...
```

In questo esempio si è scelto `/dev/sdb`. 

Per creare l'immagine usiamo 
```bash
sudo dd if=/dev/sdb of=/path_of_image/ssd240gb.img conv=noerror
```
e aspettiamo che finisca.

## Montare l'immagine con il software `kpartx`
Per montare l'immagine è necessario creare un device per ogni partizione, ciò lo si effettua con il tool `kpartx`.  
Solitamente non è preinstallato nelle distribuzioni. Per installarlo, attraverso il package manager in uso, bisogna dare il comando 
```bash
sudo apt/yum/dnf install kpartx
```
Per eseguirlo lo si fa nel seguente modo
```bash
sudo kpartx -a -v /path_of_image/ssd240gb.img
add map loop6p1 (253:0): 0 1185792 linear 7:6 2048
add map loop6p2 (253:1): 0 364713984 linear 7:6 1187840
add map loop6p3 (253:2): 0 83886080 linear 7:6 365901824
add map loop6p4 (253:3): 0 50329600 linear 7:6 449787904
```
Come si vede dall'output, nel caso dell'esempio, sono stati creati 4 dispositivi `loop*p*` nel percorso `/dev/mapper/` che possono essere montati come qualsiasi partizione. Ossia con 
```bash
sudo mount /dev/mapper/loop6p2 /mnt/part1
```
<br><br>   
Una volta terminato il lavoro bisogna smontare le partizioni

```bash
umount /mnt/part1
```
e eliminare tali dispositivi con il comando 

```bash
kpartx -d -v /path_of_image/ssd240gb.img
```