﻿---
title: "How to create a theme for Hugo with support for multilingual data files"
date: 2020-06-20T16:30:30+02:00
draft: false
featured: true
imgsrc: "img/HugoMultilingualDataFiles.png"
---
![HugoMultilingualDataFiles](/img/HugoMultilingualDataFiles.png)  
In this guide I will explain how to create a Hugo theme page that displays data from the `data` folder for a multilingual site.

In order for this to be possible, the versions of the files in the `data` folder must be placed in subfolders with the language code as their name. For example if we have a multilingual site in Italian and English and the file has the name `bio.json`, we will have:
- `data/it/bio.json`
- `data/en/bio.json`

The theme page is composed as follows:
1. The declaration of a variable that refers to the folder relative to the displayed language and its assignment through the function [index](https://gohugo.io/functions/index-function/).  
````{{ $data := index .Site.Data $.Site.Language.Lang }}````
2. Beginning of the cycle by referring to a specific data file.  
```{{ range $data.bio }}```
3. Definition of the html code to display the various contents of the file
4. Closing the cycle with the following instruction  
```{{ end }}```

An example of this page can be found at this [link](https://gitlab.com/mgiorgio25/mgiorgio25.gitlab.io/-/blob/master/layouts/partials/portfolio/experience.html)