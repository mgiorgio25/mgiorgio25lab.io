﻿---
title: "Come realizzare un tema per Hugo con il supporto ai file data multilingua"
date: 2020-06-20T16:30:30+02:00
draft: false
featured: true
imgsrc: "img/HugoMultilingualDataFiles.png"
---
![HugoMultilingualDataFiles](/img/HugoMultilingualDataFiles.png)  
In questa guida andrò a spiegare come creare una pagina del tema per Hugo che visualizzi dati provenienti dalla cartella `data` per un sito multilingua.

Affinché questo sia possibile, nella cartella `data` le versioni dei file devono essere posizionati in sottocartelle aventi come nome il codice della lingua. Per esempio se abbiamo un sito multilingua in italiano e inglese e il file ha nome `bio.json`, avremo:
- `data/it/bio.json`
- `data/en/bio.json`

La pagina del tema è composta nel seguente modo:
1. La dichiarazione di una variabile che fa riferimento alla cartella relativa alla lingua visualizzata e la sua assegnazione attraverso la funzione [index](https://gohugo.io/functions/index-function/).  
````{{ $data := index .Site.Data $.Site.Language.Lang }}````
2. Inizio del ciclo facendo riferimento a un determinato file di dati.  
```{{ range $data.bio }}```
3. Definizione del codice html per visualizzare i vari contenuti del file
4. Chiusura del ciclo con la seguente istruzione  
```{{ end }}```

Un esempio di questa pagina lo potete trovare a questo [link](https://gitlab.com/mgiorgio25/mgiorgio25.gitlab.io/-/blob/master/layouts/partials/portfolio/experience.html)