{
  "title": "Python Task Scheduler",
  "date": "2021-10-09T12:41:05-05:00",
  "draft": false,
  "image": "/img/icon_task_scheduler.png",
  "imgsrc": "img/icon_task_scheduler.png",
  "link": "https://github.com/Tecnomiky/TaskScheduler",
  "tags": ["Python", "cron", "shell"],
  "fact": "",
  "featured":true
}

The "Python Task Scheduler" is a task scheduler to plan the execution of others software in particular certain moment or on the basis of some intervals. The description of it works is specified by a text file based on basic cron syntax, a Linux software. 

<!--more-->

That is:
* The top 5 fields represent the "temporal condition" and are, in the order:
  1. minute of execution (0-59)
  2. hour of execution (0-23)
  3. day of the month (1-31)
  4. month (1-12)
  5. day of the week (0-6 where 0 is sunday) 
* The sixth field of line is the command to execute (with addition of eventual output redirect). 

For the top 5 fields you can use also multiple value through these operators:
* comma (,): define a values list (for example: 1,2,5,8)
* dash (-): define a values range (for example: 1-4 correspond to 1,2,3,4)
* asterisk (*): define every possible values (if utilized in the minute field, for instance, mean "every minute")
* slash (/): is utilized in combination a asterisk to create periodic repetitions (for instance */3 in the day field means every three days). 

An example follows of a scheduled operation executed at midnight of every saturday:
```
0 0 * * 6 /script/my_command.sh >/dev/null 2>&1
```
