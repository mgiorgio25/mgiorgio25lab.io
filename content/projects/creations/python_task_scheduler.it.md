{
  "title": "Python Task Scheduler",
  "date": "2021-10-09T12:41:05-05:00",
  "draft": false,
  "image": "/img/icon_task_scheduler.png",
  "imgsrc": "img/icon_task_scheduler.png",
  "link": "https://github.com/Tecnomiky/TaskScheduler",
  "tags": ["Python", "cron", "shell"],
  "fact": "",
  "featured":true
}

Python Task Scheduler è un task scheduler ossia un software per pianificare l'esecuzione di altri software in certi momenti o secondo certi intervalli. La descrizione di cosa eseguire è specificata attraverso un file di testo che usa la sintassi base di cron, un software presente in Linux.

<!--more-->

Essa è:
* i primi 5 campi rappresentano la "condizione temporale" e sono, nell'ordine:
  1. minuto di esecuzione (0-59)
  2. ora di esecuzione (0-23)
  3. giorno del mese (1-31)
  4. mese (1-12)
  5. giorno della settimana (0-6 dove 0 è domenica)
* il sesto campo della linea è il comando da eseguire (con l'aggiunta di una eventuale indicazione circa il redirect dell'output). 

Per i primi 5 campi è possibile utilizzare anche valori multipli utilizzando i seguenti operatori:
* virgola (,): definisce una lista di valori (ad esempio: 1,2,5,8)
* trattino (-): definisce un range di valori (ad esempio: 1-4 corrisponde a 1,2,3,4)
* asterisco (*): definisce ogni valore possibile (se utilizzato nel campo minuto, ad esempio, significa "ogni minuto")
* barra (/): è utilizzato in combinazione all'asterisco per creare ripetizioni periodiche (ad esempio */3 nel campo dei giorni vuol dire ogni tre giorni). 

Segue un esempio di operazione pianificata eseguita a mezzanotte di ogni sabato:
```
0 0 * * 6 /script/mio_comando.sh >/dev/null 2>&1
```
