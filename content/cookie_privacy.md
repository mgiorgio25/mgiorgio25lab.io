{
  "title": "Cookie e Privacy",
  "date": "2022-05-13T16:00:00",
  "draft": false,
  "image": "",
  "fact": "",
  "featured":false
}

## Privacy e informativa sull’uso dei dati
Rispetto in modo religioso la tua privacy e sono consapevole dei tuoi diritti secondo la legge applicabile, incluse le leggi in attuazione delle Direttiva Europea per la Protezione dei Dati Personali e la Direttiva E-Privacy.  

Uso i tuoi dati (nome ed email) solo ed esclusivamente per le finalità per cui me li concedi e mai per altri motivi o, peggio, venendoli o cedendoli a fini di lucro.

## Navigazione sul sito
Il bello di avere un blog è (anche) scoprire quante persone mi leggono, quante pagine vengono aperte, da dove provengono i lettori, e altre cosine di questo tipo.

Per ottenere queste informazioni uso [Google Analytics](http://www.google.com/analytics/), che è un servizio gratuito fornito da Google.

Le informazioni generate dal cookie di Google Analytics sull’utilizzo del sito web da parte tua vengono trasmesse a Google e depositate presso i suoi server negli Stati Uniti.

Google usa queste informazioni per tracciare l’uso del sito da parte dei visitatori e fornire questi dati, in forma aggregata e mai collegati alla tua identità, attraverso i report di Analytics e i suoi altri servizi informativi (es. Google Trends).

Google potrebbe trasferire queste informazioni a terzi nel caso in cui questo sia imposto dalla legge o nel caso in cui si tratti di soggetti che trattano queste informazioni per suo conto; [l’informativa su privacy e uso dei dati di Google Analytics è qui](http://www.google.it/intl/it/policies/).

Io uso i dati delle analytics per capire meglio cosa ti piace e cosa non ti interessa, e per migliorare i contenuti di questo blog.

Se non vuoi essere “tracciato”, nemmeno in forma anonima, da questo o da altri siti che usano Google Analytics (ce ne sono milioni online…), puoi scaricare il [componente aggiuntivo per disattivare l’invio dei dati di navigazione a Google Analytics](http://goo.gl/E5fFs): funziona con i browser più diffusi, ed è distribuito gratuitamente da Google.

## In conclusione
Se hai letto fino qui tutta questa pappardella sulla privacy (e non è ancora finita!), senza annoiarti troppo, non mi resta che farti i complimenti

L’attenzione alla privacy per me è molto importante, ma sarebbe ancora meglio se nel nostro paese non rendesse la vita complicata alle persone, invece di migliorarla.

Se hai ancora tempo qui sotto trovi tutta la policy estesa, e scritta in “legalese”, su privacy, cookies e affini.