---
title: "Biografia"
date: 2020-04-28T17:30:36+02:00
draft: false
---

Si sperimenta, sin da ragazzo, come autodidatta e apprendista volontario, nel mondo della
tecnologia, specialmente dell'informatica. Approfondisce inizialmente la conoscenza del
contesto informatico, attraverso la lettura e lo studio di riviste specializzate, blog e forum.  
A partire dal secondo anno della scuola secondaria di secondo grado a indirizzo Informatica si dedica
prevalentemente all'ambito delle reti informatiche, dallo sviluppo alla gestione. In particolare
attraverso la realizzazione di ambienti client-server tramite l'utilizzo di macchine virtuali (VM).
Inoltre studia e sperimenta il processo che porta allo sviluppo del software, tramite progetti
personali, scolastici e universitari.